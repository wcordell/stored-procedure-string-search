
import GMTRef

test_GMTRef_class = False
test_SQL_class = False
test_ProdCustomer_class = True
test_GDWCustomer_class = True
test_ConsultingCustomer_class = False

if test_GMTRef_class:
    print('Testing: GMTRef.GMTRef.get_customer_key_from_name()')
    print('Results: ')
    # Input: Customer Name
    # Output: Customer Key
    print(GMTRef.GMTRef.get_customer_key_from_name('Abercrombie & Fitch'))
    print('\n')

    print('Testing: GMTRef.GMTRef.get_customer_name_list()')
    print('Results: ')
    # Input: <none>
    # Output: List of customer names
    print(GMTRef.GMTRef.get_customer_name_list())
    print('\n')

    print('Testing: GMTRef.GMTRef.get_customer_prod_location()')
    print('Results: ')
    # Input: Customer Key
    # Output: List of customer names
    print(GMTRef.GMTRef.get_customer_prod_location('11385'))
    print('\n')

    print('Testing: GMTRef.GMTRef.get_customer_gdw_location()')
    print('Results: ')
    # Input: Customer Key
    # Output: List of customer names
    print(GMTRef.GMTRef.get_customer_gdw_location('11385'))
    print('\n')

    print('Testing: GMTRef.GMTRef.get_databases()')
    # Input: Server Name
    # Output: List of tuples with database names and server names
    for server in GMTRef.GMTRef.get_all_servers():
        print(f'Results for Server: {server}')
        print(GMTRef.GMTRef.get_databases(server))
        print('\n')
    print('\n')

    # What to do on this one?? <RESEARCH>
    print('Testing: GMTRef.GMTRef.get_consulting_work_databases()')
    print('Results: ')
    # Input: <none>
    # Output: List of consulting database names
    print(GMTRef.GMTRef.get_consulting_work_databases())
    print('\n')

    print('Testing: GMTRef.GMTRef.get_server_from_database()')
    print('Results: ')
    # Input: Database Name
    # Output: List of server names
    print(GMTRef.GMTRef.get_server_from_database('Abercrombie_Fitch'))
    print('\n')

    print('Testing: GMTRef.GMTRef.get_all_servers()')
    print('Results: ')
    # Input: <none>
    # Output: List of server names
    print(GMTRef.GMTRef.get_all_servers())
    print('\n')


if test_SQL_class:
    print('Testing: GMTRef.SQL.get_query_results()')
    print('Results: ')
    # Input: Server Name, Database Name, Query String
    # Output: List of SQL results
    server = 'DB27'
    print(GMTRef.SQL.get_query_results(server, "master", "SELECT name,'" + server + "' FROM master.dbo.sysdatabases"))
    print('\n')

    print('Testing: GMTRef.SQL.get_query_results_df()')
    print('Results: ')
    # Input: Server Name, Database Name, Query String
    # Output: Dataframe of SQL results
    server = 'DB27'
    print(GMTRef.SQL.get_query_results_df(server, "master", "SELECT name,'" + server + "' FROM master.dbo.sysdatabases"))
    print('\n')

    print('Testing: GMTRef.SQL.get_query_results_headers()')
    print('Results: ')
    # Input: Server Name, Database Name, Query String
    # Output: List of column names
    server = 'DB27'
    print(GMTRef.SQL.get_query_results_headers(server, "master", "SELECT name,'" + server + "' FROM master.dbo.sysdatabases"))
    print('\n')

    print('Testing: GMTRef.SQL.execute_query()')
    print('Results: ')
    # Input: Server Name, Database Name, Query String
    # Output: <none>
    server = 'DB27'
    print(GMTRef.SQL.execute_query(server, "master", "SELECT name,'" + server + "' FROM master.dbo.sysdatabases"))
    print('\n')


if test_ProdCustomer_class:
    query = 'select top 10 GRN03_Airbill_Key from vAirbillAll'
    customer_name = 'ABC Wall Coverings'

    print('Testing: GMTRef.ProdCustomer.__init__')
    # Input: Customer Name
    # Output: An initiated class object
    customer = GMTRef.ProdCustomer(customer_name)
    print(f'Type: {type(customer)}')
    print(f'Customer Name: {customer.name}')
    print(f'Customer Number: {customer.customer_number}')
    print(f'Server Name: {customer.server}')
    print(f'Database Name: {customer.db}')
    print('\n')

    print('Testing: GMTRef.ProdCustomer.query_results()')
    print('Results: ')
    # Input: Query String
    # Output: List of SQL results
    print(customer.query_results(query))
    print('\n')

    print('Testing: GMTRef.ProdCustomer.query_results_df()')
    print('Results: ')
    # Input: Query String
    # Output: Dataframe of SQL results
    print(customer.query_results_df(query))
    print('\n')

    print('Testing: GMTRef.ProdCustomer.query_headers()')
    print('Results: ')
    # Input: Query String
    # Output: List of column names
    print(customer.query_headers(query))
    print('\n')

    print('Testing: GMTRef.ProdCustomer.execute_query()')
    print('Results: ')
    # Input: Query String
    # Output: <none>
    print(customer.execute_query(query))
    print('\n')


if test_GDWCustomer_class:
    query = 'select top 10 Shipment_Key from Shipments'
    customer_name = 'ABC Wall Coverings'

    print('Testing: GMTRef.GDWCustomer.__init__')
    # Input: Customer Name
    # Output: An initiated class object
    customer = GMTRef.GDWCustomer(customer_name)
    print(f'Type: {type(customer)}')
    print(f'Customer Name: {customer.name}')
    print(f'Customer Number: {customer.number}')
    print(f'Server Name: {customer.server}')
    print(f'Database Name: {customer.db}')
    print('\n')

    print('Testing: GMTRef.GDWCustomer.query_results()')
    print('Results: ')
    # Input: Query String
    # Output: List of SQL results
    print(customer.query_results(query))
    print('\n')

    print('Testing: GMTRef.GDWCustomer.query_results_df()')
    print('Results: ')
    # Input: Query String
    # Output: Dataframe of SQL results
    print(customer.query_results_df(query))
    print('\n')

    print('Testing: GMTRef.GDWCustomer.execute_query()')
    print('Results: ')
    # Input: Query String
    # Output: <none>
    print(customer.execute_query(query))
    print('\n')

# What to do on this one?? <RESEARCH>
# if test_GDWCustomer_class:
#     query = 'select top 10 Shipment_Key from Shipments'
#
#     print('Testing: GMTRef.GDWCustomer.__init__')
#     # Input: Customer Name
#     # Output: An initiated class object
#     customer = GMTRef.GDWCustomer('Abercrombie & Fitch')
#     print(f'Type: {type(customer)}')
#     print(f'Customer Name: {customer.name}')
#     print(f'Customer Number: {customer.number}')
#     print(f'Server Name: {customer.server}')
#     print(f'Database Name: {customer.db}')
#     print('\n')
#
#     print('Testing: GMTRef.GDWCustomer.query_results()')
#     print('Results: ')
#     # Input: Query String
#     # Output: List of SQL results
#     print(customer.query_results(query))
#     print('\n')
#
#     print('Testing: GMTRef.GDWCustomer.query_results_df()')
#     print('Results: ')
#     # Input: Query String
#     # Output: Dataframe of SQL results
#     print(customer.query_results_df(query))
#     print('\n')
#
#     print('Testing: GMTRef.GDWCustomer.execute_query()')
#     print('Results: ')
#     # Input: Query String
#     # Output: <none>
#     print(customer.execute_query(query))
#     print('\n')


