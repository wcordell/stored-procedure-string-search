import decimal
import pymssql
from pandas import read_sql


# use this to get customer number once the user have selected a customer name
class GMTRef:
    @staticmethod
    def get_customer_key_from_name(customer_name):
        try:
            server = "GreenMtn"
            user = "JoshW_Dev"
            password = "V8Local$"
            database = "GreenMtn"
            proc = "dbo.GetCustomerListandStatus"
            customer_name_key_list = []

            with pymssql.connect(server, user, password, database) as conn:
                with conn.cursor() as cursor:
                    cursor.callproc(proc)
                    cursor.nextset()
                    results = cursor.fetchall()
                    conn.commit()
                    for result in results:
                        if result[2] == "Production":
                            customer_name_key_list.append(result[0])
                            customer_name_key_list.append(result[1])

            customer_key = customer_name_key_list[customer_name_key_list.index(customer_name) - 1]
        except ValueError as ve:
            print(customer_name + " is not a recognized customer name")
            exit()
        return customer_key

    @staticmethod
    def get_customer_name_list():
        server = "GreenMtn"
        user = "JoshW_Dev"
        password = "V8Local$"
        database = "GreenMtn"
        proc = "dbo.GetCustomerListandStatus"
        customer_name_list = []

        with pymssql.connect(server, user, password, database) as conn:
            with conn.cursor() as cursor:
                cursor.callproc(proc)
                cursor.nextset()
                results = cursor.fetchall()
                conn.commit()
                for result in results:
                    if result[2] == "Production":
                        customer_name_list.append(result[1])
        return customer_name_list

    # get the production server and db for a customer with the key
    @staticmethod
    def get_customer_prod_location(customer_number):
        i = 0
        results = SQL.get_query_results("GreenMtn", "GreenMtn",
                                        "SELECT Server_Name, Customer_Database_Name FROM Customer_Pathname where customer_key = '" + str(
                                            customer_number) + "'")
        for result in results:
            if i == 0:
                customer_server = result[0]
                customer_db = result[1]
                i = i + 1

        return customer_server, customer_db

    # get the GDW server and db for a customer with the key
    @staticmethod
    def get_customer_gdw_location(customer_number):
        i = 0
        results = SQL.get_query_results("GreenMtn", "GreenMtn",
                                        "SELECT SUBSTRING(Reporting_Database_Name, CHARINDEX('=', Reporting_Database_Name, 0)+1, "
                                        "ABS(CHARINDEX('.', Reporting_Database_Name, 0)-CHARINDEX('=', Reporting_Database_Name, "
                                        "0)-1)),RIGHT(Reporting_Database_Name, ABS(CHARINDEX('=', "
                                        "REVERSE(Reporting_Database_Name))-1)) FROM Customer_Pathname where customer_key = '" +
                                        str(customer_number) + "'")
        for result in results:
            if i == 0:
                customer_server = result[0]
                customer_db = result[1]
                i = i + 1

        return customer_server, customer_db

    @staticmethod
    def get_databases(server):
        databases = []
        results = SQL.get_query_results(server, "master", "SELECT name,'" + server + "' FROM master.dbo.sysdatabases")
        for result in results:
            databases.append(result)
        return databases

    @staticmethod
    def get_consulting_work_databases():
        databases = []
        db20_databases = GMTRef.get_databases("DB20")
        for db in db20_databases:
            databases.append(db[0])

        db25_databases = GMTRef.get_databases("GreenMtn")
        for db in db25_databases:
            databases.append(db[0])

        consulting_databases = []
        for db in databases:
            if db.find("Consulting_") >= 0 and db.find("_work") >= 0:
                consulting_databases.append(db)

        return consulting_databases

    @staticmethod
    def get_server_from_database(database):
        server_db_list = []
        servers = GMTRef.get_all_servers()
        for server in servers:
            databases = GMTRef.get_databases(server)
            for db in databases:
                server_db_list.append(db)
        for records in server_db_list:
            if records[0] == database:
                return records[1]

    @staticmethod
    def get_all_servers():
        # servers = ["DB20", "DB25", "DW1", "DW2", "DW3"]
        servers = ["DB27", "DB28", "DB26", "DB29", "DB30"]
        return servers


class SQL:
    @staticmethod
    def get_query_results(server, db, query):
        with pymssql.connect(server=server, database=db) as conn:
            with conn.cursor() as cursor:
                cursor.execute(query)
                results = cursor.fetchall()
                conn.commit()
                conn.close()
        return results

    @staticmethod
    def get_query_results_df(server, db, query):
        with pymssql.connect(server=server, database=db) as conn:
            results = read_sql(query, conn)
            conn.commit()
            conn.close()
        return results

    @staticmethod
    def get_query_results_headers(server, db, query):
        with pymssql.connect(server=server, database=db) as conn:
            with conn.cursor() as cursor:
                cursor.execute(query)
                colNameList = []
                for i in range(len(cursor.description)):
                    desc = cursor.description[i]
                    colNameList.append(desc[0])
                conn.commit()
                conn.close()
        return colNameList

    @staticmethod
    def execute_query(server, db, query):
        with pymssql.connect(server=server, database=db) as conn:
            with conn.cursor() as cursor:
                cursor.execute(query)
                conn.commit()
                conn.close()


class ProdCustomer:
    def __init__(self, customer_name):
        self.name = customer_name
        try:
            self.customer_number = GMTRef.get_customer_key_from_name(customer_name)
        except ValueError:
            print(customer_name + "was not a recognized customer name")
            raise Exception
        self.server, self.db = GMTRef.get_customer_prod_location(self.customer_number)

    def __str__(self):
        return self.customer_name

    def query_results(self, query):
        results = SQL.get_query_results(self.server, self.db, query)
        return results

    def query_results_df(self, query):
        results = SQL.get_query_results_df(self.server, self.db, query)
        return results

    def query_headers(self, query):
        headers = SQL.get_query_results_headers(self.server, self.db, query)
        return headers

    def execute_query(self, query):
        SQL.execute_query(self.server, self.db, query)


class GDWCustomer:
    def __init__(self, customer_name):
        self.number = GMTRef.get_customer_key_from_name(customer_name)
        self.name = customer_name
        self.server, self.db = GMTRef.get_customer_gdw_location(self.number)

    def query_results(self, query):
        results = SQL.get_query_results(self.server, self.db, query)
        return results

    def query_results_df(self, query):
        results = SQL.get_query_results_df(self.server, self.db, query)
        return results

    def execute_query(self, query):
        SQL.execute_query(self.server, self.db, query)


class ConsultingCustomer:
    def __init__(self, consulting_name):
        self.name = consulting_name
        self.db = consulting_name
        self.server = GMTRef.get_server_from_database(consulting_name)
        self.father_db = consulting_name.replace('_work', '')
        self.master_airbill = self.get_master_airbill_from_name()

    def query_results(self, query):
        results = SQL.get_query_results(self.server, self.db, query)
        return results

    def query_results_df(self, query):
        results = SQL.get_query_results_df(self.server, self.db, query)
        return results

    def execute_query(self, query):
        SQL.execute_query(self.server, self.db, query)

    def get_master_airbill_from_name(self):
        results = SQL.get_query_results(self.server, self.father_db,
                                        " Select distinct tab.name as table_name from sys.tables as tab inner join sys.columns as col on tab.object_id = col.object_id left join sys.types as t on col.user_type_id = t.user_type_id Where tab.name like ('%_Master_Airbill') order by table_name")
        for result in results:
            airbill_table = result[0]
            return airbill_table
