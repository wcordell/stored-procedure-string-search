# -*- coding: utf-8 -*-
"""
Author: William Cordell
Date Created: Wed Nov 27, 2019

Description:


Steps:
    - ()

Data Structures used:

    -
"""
import sys
import os
import tkinter as tk
from datetime import datetime
from tkinter import font as tkfont
from tkinter.filedialog import askdirectory

import openpyxl

import GMTRef
import bsefunc as bse


class Application(tk.Tk):

    def __init__(self, *args, **kwargs):
        '''

        :param args:
        :param kwargs:
        '''
        tk.Tk.__init__(self, *args, **kwargs)

        # This section sets up the main application window.
        self.title_font = tkfont.Font(family='Helvetica', size=18, weight="bold", slant="italic")
        self.text_box_font = tkfont.Font(family='Calibri', size=11)
        self.title('Stored Procedure Search')
        self.geometry('510x350')  # width by height
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        # The container is where we'll stack a bunch of frames on top of each other, then the one we want visible
        # will be raised above the others.
        container = tk.Frame(self)
        container.grid(row=0, column=0, sticky="nsew")
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        # Put all of the pages in the same location; the one on the top of the stacking order
        # will be the one that is visible.
        self.frames = {}
        for F in (PageOne, Complete):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame

            frame.grid(row=0, column=0, padx=10, pady=10, sticky="nsew")
            frame.grid_rowconfigure(0, weight=1)
            frame.grid_columnconfigure(0, weight=1)

        self.show_frame("PageOne")

    def show_frame(self, page_name):
        '''

        :param page_name:
        :return:
        '''
        frame = self.frames[page_name]
        frame.tkraise()


class PageOne(tk.Frame):

    def __init__(self, parent, controller):
        """

        :param parent:
        :param controller:
        """
        tk.Frame.__init__(self, parent)
        self.controller = controller

        self.lookup_values = []
        self.save_path = ''
        self.cust_list = []
        # Path to customer and database pairings lookup
        self.customer_lookup_values_df = GMTRef.GMTRef.get_customer_name_list()

        self.layout_frame = tk.Frame(self)
        self.layout_frame.grid(row=0, column=0, sticky="nw")
        self.layout_frame.grid_rowconfigure(0, weight=1)
        self.layout_frame.grid_columnconfigure(0, weight=1)

        self.navigation_frame = tk.Frame(self)
        self.navigation_frame.grid(row=1, column=0, sticky="se")
        self.navigation_frame.grid_rowconfigure(0, weight=1)
        self.navigation_frame.grid_columnconfigure(0, weight=1)

        ## LOOKUP VALUES
        self.lookup_values_label = tk.Label(self.layout_frame,
                                            text='Lookup values (comma separated):')
        self.lookup_values_label.grid(row=0, column=0, sticky='w')
        self.lookup_values_textBox = tk.Text(self.layout_frame,
                                             height=1,
                                             width=60,
                                             font=controller.text_box_font)
        self.lookup_values_textBox.grid(row=1, column=0)

        ## LOOKUP PRODUCTION CHECKBOXES
        self.prodbox_value = tk.StringVar()
        self.prodbox_value.set('Yes')
        self.lookup_prodbox_checkBox = tk.Checkbutton(self.layout_frame,
                                                      variable=self.prodbox_value,
                                                      onvalue='Yes',
                                                      offvalue='No',
                                                      text="Search Production",
                                                      font=controller.text_box_font)
        self.lookup_prodbox_checkBox.grid(row=2, column=0, sticky='w')

        ## LOOKUP GDW CHECKBOXES
        self.gdwbox_value = tk.StringVar()
        self.gdwbox_value.set('Yes')
        self.lookup_gdwbox_checkBox = tk.Checkbutton(self.layout_frame,
                                                     variable=self.gdwbox_value,
                                                     onvalue='Yes',
                                                     offvalue='No',
                                                     text="Search GDW",
                                                     font=controller.text_box_font)
        self.lookup_gdwbox_checkBox.grid(row=3, column=0, sticky='w')

        ## SAVE PATH
        self.save_path_label = tk.Label(self.layout_frame,
                                        text='Results save path:')
        self.save_path_label.grid(row=4, column=0, sticky='w')
        self.save_path_textBox = tk.Text(self.layout_frame,
                                         height=1,
                                         width=60,
                                         font=controller.text_box_font)
        self.save_path_textBox.grid(row=5, column=0)
        self.save_path_textBox.insert('1.0', str(os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')))
        self.save_path = str(os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop'))
        self.save_path_button = tk.Button(self.layout_frame,
                                          text="Browse...",
                                          width=9,
                                          command=self.get_save_path)
        self.save_path_button.grid(row=5, column=1, padx=5)

        ## CUSTOMER SELECTION
        self.cust_label = tk.Label(self.layout_frame,
                                   text='Select customers:')
        self.cust_label.grid(row=6, column=0, sticky='w')
        self.cust_listBox = tk.Listbox(self.layout_frame,
                                       selectmode='multiple',
                                       height=6,
                                       width=60,
                                       font=controller.text_box_font)
        self.cust_listBox.bind('<<ListboxSelect>>', self.curselect)
        self.cust_listBox.grid(row=7, column=0)

        for customer in self.customer_lookup_values_df:
            self.cust_listBox.insert('end', customer)

            ## NAVIGATION
        self.search_button = tk.Button(self.navigation_frame,
                                       text="Search",
                                       width=9,
                                       command=lambda: [self.get_lookup_values(),
                                                        self.report_search(self.save_path_textBox.get("1.0", 'end-1c'), self.lookup_values,
                                                                           self.prodbox_value.get(),
                                                                           self.gdwbox_value.get()),
                                                        controller.show_frame("Complete"),
                                                        self.clear_text()])
        self.search_button.grid(row=0, column=0, padx=10)

        self.quit_button = tk.Button(self.navigation_frame,
                                     text="Quit",
                                     width=9,
                                     command=self.close_program)
        self.quit_button.grid(row=0, column=1, padx=10)

    def report_search(self, path: str, lookupvalues: list, prod_answer: str, gdw_answer: str):
        """

        :param path: str
            Environment path where results will be saved.
        :param lookupvalues: list
            A list of string values entered by the user to search for.
        :param prod_answer: str
            Yes or No value which controls whether or not stored procedures on the production server are searched.
        :param gdw_answer: str
            Yes or No value which controls whether or not stored procedures on the GDW server are searched.
        :return:
        """
        testing = True
        start_time_all = datetime.now()

        try:
            path_delim = path.split('C:')[1].split('Users')[0]
        except:
            path_delim = '/'

        save_file_name = path + path_delim + str(start_time_all.year) + str(start_time_all.month) + str(start_time_all.day)\
                         + "_" + str(start_time_all.hour) + str(start_time_all.minute) + str(start_time_all.second)\
                         + "_Report_Search.xlsx"

        # Control Variables
        search_production = prod_answer
        search_gdw = gdw_answer

        # Workbook creation and setup
        wb = openpyxl.Workbook()
        sheet = wb.active
        # Header creation
        sheet.cell(row=1, column=1, value='Customer_Name')
        sheet.cell(row=1, column=2, value='Server')
        sheet.cell(row=1, column=3, value='Database')
        sheet.cell(row=1, column=4, value='SP_Schema')
        sheet.cell(row=1, column=5, value='SP_Name')
        sheet.cell(row=1, column=6, value='Report_Key')
        sheet.cell(row=1, column=7, value='Report_Type')
        sheet.cell(row=1, column=8, value='File_Name')
        sheet.cell(row=1, column=9, value='Description')
        sheet.cell(row=1, column=10, value='Schedule')
        sheet.cell(row=1, column=11, value='Total_Match_Count')
        next_column_pos = 12
        for value in lookupvalues:
            sheet.cell(row=1, column=next_column_pos, value=f'{value}_Match_Count')
            next_column_pos += 1
        currentrow = 2
        try:
            wb.save(save_file_name)
        except:
            print("Not able to save file")
            exit()

        # Loop over each customer in the lookup
        for customer in self.cust_list:
            start_time_CUST = datetime.now()
            print(f"Running on {customer}...")

            if testing:
                # test_amts = [1, 10, 100, 500]
                test_amts = [1, 10, 100]
                for amt in test_amts:
                    print(f"Test count. {amt}")
                    currentrow = bse.get_results(wb, save_file_name, customer, lookupvalues, sheet, currentrow, search_production, search_gdw, amt)
            else:
                currentrow = bse.get_results(wb, save_file_name, customer, lookupvalues, sheet, currentrow, search_production, search_gdw)

            print(f"Customer completed. {datetime.now() - start_time_CUST}\n")
        print(f"Everything completed. {datetime.now() - start_time_all}")

        # Final save and close
        wb.save(save_file_name)
        wb.close()

    def get_lookup_values(self):
        '''

        :return:
        '''
        lookup_values_str = self.lookup_values_textBox.get("1.0", 'end-1c')
        lookup_values_split_list = lookup_values_str.split(",")
        for value in lookup_values_split_list:
            self.lookup_values.append(value.strip())

    def get_save_path(self):
        '''

        :return:
        '''
        tk.Tk().withdraw()
        self.save_path = askdirectory()
        self.save_path_textBox.delete('1.0', 'end')
        self.save_path_textBox.insert('1.0', str(self.save_path))

    def close_program(self):
        '''

        :return:
        '''
        self.master.destroy()
        sys.exit()

    def clear_text(self):
        '''

        :return:
        '''
        self.lookup_values_textBox.delete('1.0', 'end')
        self.save_path_textBox.delete('1.0', 'end')

    def curselect(self, evt):
        '''

        :param evt:
        :return:
        '''
        self.cust_list = [self.cust_listBox.get(cust) for cust in self.cust_listBox.curselection()]


class Complete(tk.Frame):

    def __init__(self, parent, controller):
        '''

        :param parent:
        :param controller:
        '''
        tk.Frame.__init__(self, parent)
        self.controller = controller

        self.layout_frame = tk.Frame(self)
        self.layout_frame.grid(row=0, column=0, sticky="nesw")
        self.layout_frame.grid_rowconfigure(0, weight=1)
        self.layout_frame.grid_columnconfigure(0, weight=1)

        self.navigation_frame = tk.Frame(self)
        self.navigation_frame.grid(row=1, column=0, sticky="se")
        self.navigation_frame.grid_rowconfigure(0, weight=1)
        self.navigation_frame.grid_columnconfigure(0, weight=1)

        self.label = tk.Label(self.layout_frame,
                              text="Search Complete!",
                              font=controller.title_font)
        self.label.grid(row=0, column=0)

        ## NAVIGATION
        self.replace_button = tk.Button(self.navigation_frame,
                                        text="Look Again",
                                        width=9,
                                        command=lambda: controller.show_frame("PageOne"))
        self.replace_button.grid(row=0, column=0, padx=10)

        self.quit_button = tk.Button(self.navigation_frame,
                                     text="Quit",
                                     width=9,
                                     command=self.close_program)
        self.quit_button.grid(row=0, column=1, padx=10)

    def close_program(self):
        '''

        :return:
        '''
        self.master.destroy()
        sys.exit()


if __name__ == "__main__":
    app = Application()
    app.mainloop()
