# -*- coding: utf-8 -*-
"""
Author: William Cordell
Date Created: Tues Aug 10, 2020

Description:
"""
from collections import Counter
from datetime import datetime
import GMTRef
from pandas import DataFrame, concat
from numpy import repeat
from pymssql import OperationalError
from tqdm import tqdm
from math import ceil, fsum
from re import compile, escape


def sp_df_combine(df1, df2):
    # Add a label to distinguish sets of stored procedures before combining into a single table
    if df1.empty and df2.empty:
        df_combined = DataFrame()
    elif df1.empty and not df2.empty:
        df_combined = df2
    elif not df1.empty and df2.empty:
        df_combined = df1
    else:
        df_combined = concat([df1, df2], ignore_index=True)
    return df_combined


def sp_df_join(df1, df2):
    df_joined = df1.merge(df2, on='SP_Name', how='left')

    return df_joined


def get_report_schedules(customer):
    sql = (f"SELECT [ReportKey] as Report_Key,"
           f" [ReportType] as Report_Type,"
           f" [FileName] as File_Name,"
           f" [Description],"
           f" [CRS_Occurrence] as Schedule,"
           f" case when left(Stored_Procedure, 7) = 'Report.' then right(Stored_Procedure, len(Stored_Procedure)-7)"
           f" when left(Stored_Procedure, 4) = 'dbo.' then right(Stored_Procedure, len(Stored_Procedure)-4)"
           f" else Stored_Procedure end as [SP_Name]"
           f" FROM [dbo].[CustomerReportsWithSchedulesAndStoredProcedures]")

    # Create customer object
    start_time = datetime.now()
    print(f"    Retrieving schedules...")
    schedules = customer.query_results_df(sql)
    print(f"    Retrieval completed in {datetime.now() - start_time} seconds")

    return schedules


def get_report_stored_procedures(customer, server_label, test_amt: int = 0):
    if test_amt > 0:
        sql = (f"SELECT TOP (1) ROUTINE_SCHEMA as SP_Schema,"
               f" ROUTINE_NAME as SP_Name"
               f" FROM INFORMATION_SCHEMA.ROUTINES"
               f" WHERE ROUTINE_TYPE = 'PROCEDURE'")
    else:
        sql = (f"SELECT ROUTINE_SCHEMA as SP_Schema,"
               f" ROUTINE_NAME as SP_Name"
               f" FROM INFORMATION_SCHEMA.ROUTINES"
               f" WHERE ROUTINE_TYPE = 'PROCEDURE'")

    # Create customer object
    start_time = datetime.now()
    print(f"    Retrieving {customer.server} stored procedures...")
    storedprocedures = customer.query_results_df(sql)
    storedprocedures['Server'] = server_label
    print(f"    Retrieval completed in {datetime.now() - start_time} seconds")

    if test_amt > 0:
        storedprocedures = DataFrame(repeat(storedprocedures.values, test_amt, axis=0), columns=storedprocedures.columns)
    else:
        pass

    return storedprocedures


def chunker(df: DataFrame, size: int):
    """Takes a Dataframe and returns a Generator of Dataframes divided into even sections.

    :param df:
        A Dataframe to divide
    :param size:
        The number of elements desired in each section
    :return: Dataframe
        A Generator of dataframes divided into even sections
    """
    return (df[pos:pos + size] for pos in range(0, len(df), size))


def insert_with_progress(workbook, file_name, df_sp_joined, customername, search_values, output_file, current_row) -> int:
    """

    :param workbook:
    :param file_name:
    :param df_sp_joined:
    :param customername:
    :param search_values:
    :param output_file:
    :param current_row:
    :return:
    """
    # Establish size of chunks.
    chunksize = max(int(len(df_sp_joined) / 10), 1)

    with tqdm(total=len(df_sp_joined)) as pbar:
        for i, cdf in enumerate(chunker(df_sp_joined, chunksize)):
            current_row = get_sp_df_text(workbook, file_name, cdf, customername, search_values, output_file, current_row)
            pbar.update(chunksize)
            tqdm._instances.clear()

    return current_row


def insert_without_progress(workbook, file_name, df_sp_joined, customername, search_values, output_file, current_row) -> int:
    """

    :param workbook:
    :param file_name:
    :param df_sp_joined:
    :param customername:
    :param search_values:
    :param output_file:
    :param current_row:
    :return:
    """

    current_row = get_sp_df_text(workbook, file_name, df_sp_joined, customername, search_values, output_file, current_row)

    return current_row


def make_fuzzy(value):
    # O(n) = fuzzy_begin_chars*fuzzy_end_chars = c
    # Edit this list to change the fuzzy search criteria
    fuzzy_begin_chars = ['*', '(', '/', '\\', ';', '[', ' ', '\n', '.', '{']
    fuzzy_end_chars = ['*', ')', '/', '\\', ';', ']', ' ', '\n', '.', '}']

    fuzzy_values = []
    for fuzzy_begin in fuzzy_begin_chars:
        for fuzzy_end in fuzzy_end_chars:
            fuzzy_values.append(fuzzy_begin + value + fuzzy_end)
    return fuzzy_values


def get_matches(sp, search_value):
    match_count = 0
    for value in search_value:
        sp = sp.upper()
        match_count += sp.count(value.upper())

    return match_count


def get_sp_df_text(workbook, file_name, df, customername, search_values, output_file, current_row):
    """

    :param workbook:
    :param file_name:
    :param df:
    :param customername:
    :param search_values:
    :param output_file:
    :param current_row:
    :return:
    """

    prod_customer = GMTRef.ProdCustomer(customername)
    gdw_customer = GMTRef.GDWCustomer(customername)

    # Loop over each stored procedure returned
    for index, sp in df.iterrows():
        # Create server connection
        if sp['Server'] == "Production":
            customer = prod_customer
        elif sp['Server'] == "GDW":
            customer = gdw_customer
        else:
            raise BaseException("Invalid server input")

        # Write stored procedure info to workbook
        output_file.cell(row=current_row, column=1, value=customer.name)
        output_file.cell(row=current_row, column=2, value=customer.server)
        output_file.cell(row=current_row, column=3, value=customer.db)
        output_file.cell(row=current_row, column=4, value=sp['SP_Schema'])
        output_file.cell(row=current_row, column=5, value=sp['SP_Name'])
        output_file.cell(row=current_row, column=6, value=sp['Report_Key'])
        output_file.cell(row=current_row, column=7, value=sp['Report_Type'])
        output_file.cell(row=current_row, column=8, value=sp['File_Name'])
        output_file.cell(row=current_row, column=9, value=sp['Description'])
        output_file.cell(row=current_row, column=10, value=sp['Schedule'])

        # Retrieve stored procedure text
        sql = f"EXEC sp_helptext '[{sp['SP_Schema']}].[{sp['SP_Name']}]'"

        try:
            # start_time_query = datetime.now()
            results = customer.query_results(sql)
            sp_text = ''
            # count = 1
            for result in results:
                # print(count)
                # print(result[0])
                sp_text += result[0]
                # count = count + 1
            # print(f"    Query results retrieved. {datetime.now() - start_time_query}")

            # start_time_matches = datetime.now()
            start_col = 12
            total_matches = 0
            # Look for each value in the text
            for value in search_values:
                fuzzy_values = make_fuzzy(value)
                matches = get_matches(sp_text, fuzzy_values)
                # Save results of value search to file
                output_file.cell(row=current_row, column=start_col, value=matches)
                start_col += 1
                total_matches += matches
            # print(f"    All Matches found. {datetime.now() - start_time_matches}")

            output_file.cell(row=current_row, column=11, value=total_matches)
            # Go to next row in workbook for next stored procedure info
            current_row += 1
        except OperationalError:
            start_col = 12
            total_matches = 0
            # Look for each value in the text
            for value in search_values:
                # Save results of value search to file
                output_file.cell(row=current_row, column=start_col, value=0)
                start_col += 1

            output_file.cell(row=current_row, column=11, value=total_matches)
            # Go to next row in workbook for next stored procedure info
            current_row += 1

        workbook.save(file_name)

    return current_row


def get_results(workbook, file_name, customername, search_values, output_file, current_row, search_production, search_GDW, test_amt: int = 0):
    """

    :param workbook:
    :param file_name:
    :param customername:
    :param search_values:
    :param output_file:
    :param current_row:
    :param search_production:
    :param search_GDW:
    :param test_amt:
    :return:
    """
    # Get stored procedure lists from Prod and GDW
    # Each if-statement returns a dataframe, empty if no results
    df_sp_prod = DataFrame()
    df_sp_gdw = DataFrame()

    if search_production == 'Yes' and search_GDW == 'Yes':
        # Create server connection
        customer = GMTRef.ProdCustomer(customername)
        df_sp_prod = get_report_stored_procedures(customer, 'Production', ceil(test_amt/2))

        # Create server connection
        customer = GMTRef.GDWCustomer(customername)
        df_sp_gdw = get_report_stored_procedures(customer, 'GDW', ceil(test_amt/2))
    elif search_production == 'Yes' and search_GDW == 'No':
        # Create server connection
        customer = GMTRef.ProdCustomer(customername)
        df_sp_prod = get_report_stored_procedures(customer, 'Production', test_amt)
    elif search_production == 'No' and search_GDW == 'Yes':
        # Create server connection
        customer = GMTRef.GDWCustomer(customername)
        df_sp_gdw = get_report_stored_procedures(customer, 'GDW', test_amt)

    df_sp_combined = sp_df_combine(df_sp_prod, df_sp_gdw)
    df_sp_schedules = get_report_schedules(GMTRef.ProdCustomer(customername))
    df_sp_joined = sp_df_join(df_sp_combined, df_sp_schedules)

    print(f"    Searching {df_sp_joined.shape[0]} stored procedures for all lookup values...")
    start_time = datetime.now()
    if test_amt > 0:
        current_row = insert_without_progress(workbook, file_name, df_sp_joined, customername, search_values, output_file, current_row)
    else:
        current_row = insert_with_progress(workbook, file_name, df_sp_joined, customername, search_values, output_file, current_row)
    print(f"    Search completed. {datetime.now() - start_time}\n")

    return current_row
